#! /usr/bin/bash

projectDir=$1

cd $projectDir/build

make && make install

cd ../bin

if !([ -x ./main ]); then
  echo main is not executable!
  exit 1
fi

./main

exit 0