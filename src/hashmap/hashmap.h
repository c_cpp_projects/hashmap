#ifndef HASHMAP_H
#define HASHMAP_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "../linked_list/linked_list.h"

#define DEFAULT_HASHMAP_CAPACITY           ((size_t) 64)
#define DEFAULT_HASHMAP_CAPACITY_INCREMENT ((size_t) 4)

typedef struct HashMapStruct* THashMap;

typedef size_t (*HashingFunction)(const void* key);
typedef bool (*CompareKeyFunction)(const void* key, const void* other,
                                   const HashingFunction hashing);
typedef void (*PrintFunction)(size_t index, const void* key, const void* element);

/**
 * @brief Add an element to this HashMap, which can be identified by key.
 *        This HashMap CANNOT contain any duplicate keys, so if the
 *        specified key is already present within this HashMap, then
 *        neither the specified key nor the element will be added to
 *        this HashMap.
 *
 *
 * @param map The HashMap you want to add an element to.
 *
 * @param key The key used for element.
 *
 * @param element Pointer to the data you want to store.
 */
void HashMapAdd(THashMap map, const void* key, const void* element);

void HashMapClear(const THashMap map, bool freeKeys, bool freeElements);

/**
 * @brief Get a new HashMap, which is a copy of source.
 *
 *
 * @param source The HashMap you want to copy from.
 *
 * @param capacity The capacity for the copy of source.
 *
 *
 * @returns A new HashMap with the entries from source.
 */
THashMap HashMapCopy(const THashMap source, size_t capacity);

/**
 * @brief Allocate memory for a new HashMap.
 *
 *
 * @param capacity The initial capacity of this HashMap. If you do not want
 *                 to customize anything in particular, it is advised to
 *                 supply DEFAULT_HASHMAP_CAPACITY for capacity.
 *                 By changing this value you can increase computation
 *                 speed when adding new values.
 *
 * @param capacityIncrement How much the capacity of this HashMap should
 *                          be increased, in case this HashMap needs to increase
 *                          its capacity. Assigning a big number to this
 *                          parameter results in more memory being used,
 *                          while setting it too low can significantly increase
 *                          the computation speed. It is advised to set it
 *                          to DEFAULT_HASHMAP_CAPACITY_INCREMENT.
 *
 * @param hashing This function is used as the hashing function for
 *                this HashMap.
 *
 * @param compareKey This function is used to check if a new key already exists in
 *                   this HashMap and to get the correct element associated with a
 *                   given key.
 *
 *
 * @returns Pointer to the newly allocated HashMapStruct.
 */
THashMap HashMapCreate(size_t initialCapacity, size_t capacityIncrement,
                       const HashingFunction hashing,
                       const CompareKeyFunction compareKey);

/**
 * @brief Destroy this HashMap.
 *
 *
 * @param map This HashMap should not be used for any other methods anymore.
 *
 * @param freeElements true if 'free()' should be apllied to every element,
 *                     otherwise false if no element should be freed.
 */
void HashMapDestroy(THashMap map, bool freeKeys, bool freeElements);

/**
 * @brief Get a value from this HashMap.
 *
 *
 * @param map The HashMap from which you want to get an element.
 *
 * @param key The key from the element you want to get.
 *
 *
 * @returns The element associated with key, or NULL if there is none.
 */
void* HashMapGet(const THashMap map, const void* key);

bool HashMapIsEmpty(const THashMap map);

bool HashMapIsNotEmpty(const THashMap map);

/**
 * @param map The HashMap you want to print to the console.
 *
 * @param print This function is used to print each key/element
 *              pair in this HashMap. If print is NULL, then a
 *              default print function is supplied.
 */
void HashMapPrint(const THashMap map, const PrintFunction print);

/**
 * @brief Associates the specified element with the specified key in this map.
 *
 *
 * @param map The HashMap you want to change an element of.
 *
 * @param element The new element you want to associate with key.
 *
 *
 * @returns The element that used to be associated with key or NULL if no
 *          element has been added to this HashMap for the specified key.
 */
void* HashMapPut(const THashMap map, const void* key, const void* element);

/**
 * @brief Change the capacity of this HashMap. You can significantly
 *        increase computation speed by setting this
 *        to an appropriate value for your usecase.
 *
 *
 * @param map The HashMap you want to change the capacity of.
 *
 * @param capacity The new capacity for this HashMap. This
 *                 cannot be 0.
 */
void HashMapSetCapacity(const THashMap map, size_t capacity);

/**
 * @brief Get the current amount of key and element pairs of
 *        this HashMap. This is not ressource expensive, because
 *        the current size of this HashMap is stored in a variable.
 *
 *
 * @returns Size of this HashMap.
 */
size_t HashMapSize(const THashMap map);

#endif  // HASHMAP_H