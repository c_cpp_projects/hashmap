#include "hashmap.h"

#define VOID_PTR(x) ((void*) x)

/******************** Declaration of Private TypeDefs ********************/

typedef struct Entry TEntry;

/******************** Declaration of Private Structs ********************/

struct HashMapStruct {
  // The size of entries. Equivalent to size of entries.
  size_t capacity;

  // How much the capacity should be incremented, in case
  // it needs to be incremented.
  size_t capacityIncrement;

  // This function is used for calculating the hash value for each key.
  HashingFunction hashing;

  // This function is used to check if a new key already exists in
  // this HashMap and to get the correct Entry associated with a
  // given key.
  CompareKeyFunction compareKey;

  // Array containing all LinkedLists for each key.
  // Every key has to have a LinkedList, which in turn
  // only has TEntry* , as a value,
  // to be able to handle a hash collision.
  TLinkedList* entries;

  // void* list containing all keys in this HashMap.
  TLinkedList keys;
};

struct Entry {
  uintptr_t key;
  uintptr_t element;
};

/******************** Declaration of Private Functions ********************/

/**
 * @brief Get a pointer to the TEntry from this HashMap associated
 *        with the specified key, or NULL if there is none.
 */
static TEntry* _HashMapGetTEntry(const THashMap map, const void* key);

/**
 * @brief Allocate enough memory for a TEntry
 *        and get the pointer to its location.
 */
static TEntry* _mallocTEntry(void);

/**
 * @brief Allocate enough memory for a copy of source
 *        and get the pointer to the copy's location.
 */
static TEntry* _copyTEntry(TEntry* source);

static void _defaultPrint(size_t index, const void* key, const void* element);

static void _freeEntryElements(size_t index, void* element);

/******************** Definition of Public Functions ********************/

void HashMapAdd(THashMap map, const void* key, const void* element) {
  for (size_t i = 0; i < ListSize(map->keys); i++) {
    // If the string key already exists in the LinkedList containing
    // all keys, or a key with the same hash value exists,
    // break the loop.
    if (map->compareKey(key, ListElementAt(map->keys, i), map->hashing)) {
      // Return if key already exists within this HashMap.

      // Use HashMapPut to change the elements of keys that are already
      // present in this HashMap.
      return;
    }
  }

  // Increase capacity of this HashMap if necessary.
  if (ListSize(map->keys) + 1 > map->capacity)
    HashMapSetCapacity(map, map->capacity + map->capacityIncrement);

  // Add the new key to this HashMap.
  ListAdd(map->keys, key);

  size_t index = map->hashing(key) % map->capacity;
  TLinkedList list = map->entries[index];
  TEntry* entry;

  entry = _mallocTEntry();
  entry->key = (uintptr_t) key;
  entry->element = (uintptr_t) element;

  ListAdd(list, entry);
}

void HashMapClear(const THashMap map, bool freeKeys, bool freeElements) {
  if (NULL == map) return;

  ListDestroy(map->keys, freeKeys);

  for (size_t i = 0; i < map->capacity; i++) {
    if (freeElements) ListForEach(map->entries[i], _freeEntryElements);
    ListClear(map->entries[i], true);
  }
}

THashMap HashMapCopy(const THashMap source, size_t capacity) {
  THashMap copy = HashMapCreate(capacity, source->capacityIncrement, source->hashing,
                                source->compareKey);

  // If there are no keys, then there also are no entries.
  if (ListIsEmpty(source->keys)) return copy;

  size_t copyIndex;
  TEntry* current;
  TLinkedList sourceList;

  for (size_t i = 0; i < source->capacity; i++) {
    sourceList = source->entries[i];

    for (size_t j = 0; j < ListSize(sourceList); j++) {
      current = _copyTEntry((TEntry*) ListElementAt(sourceList, j));
      copyIndex = source->hashing(VOID_PTR(current->key)) % capacity;
      ListAdd(copy->entries[copyIndex], current);
    }
  }

  return copy;
}

THashMap HashMapCreate(size_t initialCapacity, size_t capacityIncrement,
                       const HashingFunction hashing,
                       const CompareKeyFunction compareKey) {
  THashMap map = (THashMap) calloc(1, sizeof(struct HashMapStruct));

  map->capacity = (0 == initialCapacity) ? DEFAULT_HASHMAP_CAPACITY : initialCapacity;
  map->capacityIncrement =
      (0 == capacityIncrement) ? DEFAULT_HASHMAP_CAPACITY_INCREMENT : capacityIncrement;

  map->hashing = hashing;
  map->compareKey = compareKey;

  map->keys = ListCreate();

  map->entries = (TLinkedList*) calloc(initialCapacity, sizeof(TLinkedList));
  for (size_t i = 0; i < initialCapacity; i++) map->entries[i] = ListCreate();

  return map;
}

void HashMapDestroy(THashMap map, bool freeKeys, bool freeElements) {
  if (NULL == map) return;

  HashMapClear(map, freeKeys, freeElements);

  map->hashing = NULL;
  map->compareKey = NULL;

  free(map);
}

void* HashMapGet(const THashMap map, const void* key) {
  TEntry* entry = _HashMapGetTEntry(map, key);
  if (NULL == entry) return NULL;
  return VOID_PTR(entry->element);
}

bool HashMapIsEmpty(const THashMap map) {
  // if there are no keys, then there also are no elements
  return ListIsEmpty(map->keys);
}

bool HashMapIsNotEmpty(const THashMap map) { return !HashMapIsEmpty(map); }

void HashMapPrint(const THashMap map, const PrintFunction print) {
  if (NULL == map) {
    printf("HashMap is NULL.\n");
    return;
  }

  const PrintFunction _print = (NULL == print) ? _defaultPrint : print;

  printf("HashMapPrint\n");
  printf("capacity: %lu,\tcapacityIncrement: %lu,\tsize: %lu\n", map->capacity,
         map->capacityIncrement, HashMapSize(map));
  printf("hashing: %p,\tcompareKey: %p\n", VOID_PTR(map->hashing),
         VOID_PTR(map->compareKey));

  size_t index = 0;

  for (size_t i = 0; i < map->capacity; i++) {
    TLinkedList list = map->entries[i];
    TEntry* current;

    for (size_t j = 0; j < ListSize(list); j++) {
      current = (TEntry*) ListElementAt(list, j);
      _print(index, VOID_PTR(current->key), VOID_PTR(current->element));
      index++;
    }
  }
}

void* HashMapPut(const THashMap map, const void* key, const void* element) {
  TEntry* entry = _HashMapGetTEntry(map, key);

  if (NULL == entry) return NULL;

  uintptr_t old = entry->element;
  entry->element = (uintptr_t) element;

  return VOID_PTR(old);
}

void HashMapSetCapacity(const THashMap map, size_t capacity) {
  if (0 == capacity) return;
  if (map->capacity == capacity) return;

  TLinkedList current;

  const size_t oldEntriesSize = map->capacity;
  TLinkedList* oldEntries = (TLinkedList*) calloc(oldEntriesSize, sizeof(TLinkedList));

  // Copy all TLinkedLists from entries.
  for (size_t i = 0; i < oldEntriesSize; i++) {
    current = map->entries[i];
    oldEntries[i] = current;
  }

  // Realloc original entries and create new LinkedLists for them.
  map->capacity = capacity;
  map->entries = (TLinkedList*) realloc(map->entries, sizeof(TLinkedList) * capacity);
  for (size_t i = 0; i < capacity; i++) map->entries[i] = ListCreate();

  TEntry* entry;
  size_t index;
  for (size_t i = 0; i < oldEntriesSize; i++) {
    current = oldEntries[i];
    for (size_t j = 0; j < ListSize(current); j++) {
      entry = ListElementAt(current, j);
      index = map->hashing(VOID_PTR(entry->key)) % map->capacity;
      ListAdd(map->entries[index], entry);
    }
  }

  // Free oldEntries.
  for (size_t i = 0; i < oldEntriesSize; i++) ListDestroy(oldEntries[i], false);
  free(oldEntries);
}

size_t HashMapSize(const THashMap map) {
  // The size of keys is equivalent to the size
  // of this HashMap, because there can not be any
  // duplicate keys, so every key is associated
  // with a single element.
  return ListSize(map->keys);
}

/******************** Definition of Private Functions ********************/

static TEntry* _HashMapGetTEntry(const THashMap map, const void* key) {
  const size_t index = map->hashing(key) % map->capacity;
  TLinkedList list = map->entries[index];

  if (ListIsEmpty(list)) return NULL;

  TEntry* entry = NULL;
  TEntry* current = NULL;
  for (size_t i = 0; i < ListSize(list); i++) {
    current = (TEntry*) ListElementAt(list, i);
    if (map->compareKey(key, VOID_PTR(current->key), map->hashing)) {
      entry = current;
      break;
    }
  }

  return entry;
}

static TEntry* _mallocTEntry(void) { return (TEntry*) malloc(sizeof(TEntry)); }

static TEntry* _copyTEntry(TEntry* source) {
  TEntry* copy = _mallocTEntry();
  copy->element = source->element;
  copy->key = source->key;
  return copy;
}

static void _freeEntryElements(size_t index, void* element) { free(element); }

static void _defaultPrint(size_t index, const void* key, const void* element) {
  printf("[%lu]\tkey: %p\telement: %p\n", index, key, element);
}
