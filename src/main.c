#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "hashmap/hashmap.h"

size_t hashingFunction(const void* key);
bool compareKeyFunction(const void* key, const void* other,
                        const HashingFunction hashing);
void printFunction(size_t index, const void* key, const void* element);

void test2(void);
void test3Copy(void);
void test4Add(void);
void test5PrintFunction(void);
void test6SetCapacity(void);

int main() {
  printf("\n");

  // test2();
  // test3Copy();
  // test4Add();
  // test5PrintFunction();
  test6SetCapacity();

  return 0;
}

size_t hashingFunction(const void* key) {
  size_t hash = 0;
  for (size_t i = 0; '\0' != ((char*) key)[i]; i++)
    hash += (unsigned char) ((char*) key)[i];
  // printf("key: %s\n", key);
  // printf("hash: %lu\n", hash);
  // printf("\n");
  return hash;
}

bool compareKeyFunction(const void* key, const void* other,
                        const HashingFunction hashing) {
  if (0 == strcmp(key, (char*) other) || (hashing(key) == hashing(other))) return true;
  return false;
}

void printFunction(size_t index, const void* key, const void* element) {
  printf("[%lu] key: %s element: %d\n", index, (char*) key, *((int*) element));
}

void test2(void) {
  THashMap map = HashMapCreate(3, DEFAULT_HASHMAP_CAPACITY_INCREMENT, hashingFunction,
                               compareKeyFunction);

  int x1 = 124;
  int x2 = 420;
  int x3 = -8520;

  const char* key1 = "pen15";
  const char* key2 = "another";
  const char* key3 = "lolol";

  HashMapAdd(map, key1, &x1);
  HashMapAdd(map, key2, &x2);
  HashMapAdd(map, key3, &x3);
  // HashMapAdd(map, "pen15", &x1);
  // HashMapAdd(map, "another", &x2);
  // HashMapAdd(map, "lolol", &x2);

  int* val = (int*) HashMapGet(map, key1);
  if (NULL != val) printf("get %s: %d\n", key1, *val);
  else
    printf("Elem for %s is NULL\n", key1);

  val = (int*) HashMapGet(map, key2);
  if (NULL != val) printf("get %s: %d\n", key2, *val);
  else
    printf("Elem for %s is NULL\n", key2);

  val = (int*) HashMapGet(map, key3);
  if (NULL != val) printf("get %s: %d\n", key3, *val);
  else
    printf("Elem for %s is NULL\n", key3);

  printf("\n");
  HashMapPrint(map, printFunction);

  HashMapDestroy(map, false, false);
}

void test3Copy(void) {
  THashMap map = HashMapCreate(3, DEFAULT_HASHMAP_CAPACITY_INCREMENT, hashingFunction,
                               compareKeyFunction);

  int x1 = 124;
  int x2 = 420;
  int x3 = -8520;

  const char* key1 = "pen15";
  const char* key2 = "another";
  const char* key3 = "lolol";

  printf("Map:\n");
  HashMapPrint(map, printFunction);
  printf("*****************************************\n\n");

  HashMapAdd(map, key1, &x1);
  HashMapAdd(map, key2, &x2);
  HashMapAdd(map, key3, &x3);

  printf("Map:\n");
  HashMapPrint(map, printFunction);
  printf("*****************************************\n\n");

  printf("Copying...\n\n");
  THashMap copy = HashMapCopy(map, 5);

  printf("Destroying map...\n\n");
  HashMapDestroy(map, false, false);

  printf("Copy:\n");
  HashMapPrint(copy, printFunction);
  printf("*****************************************\n\n");

  HashMapDestroy(copy, false, false);
}

void test4Add(void) {
  THashMap map = HashMapCreate(2, 3, hashingFunction, compareKeyFunction);

  int x1 = 124;
  int x2 = 420;
  int x3 = -8520;

  const char* key1 = "pen15";
  const char* key2 = "another";
  const char* key3 = "lolol";

  HashMapAdd(map, key1, &x1);
  HashMapAdd(map, key2, &x2);

  printf("Map:\n");
  HashMapPrint(map, printFunction);
  printf("\n\n\n");

  HashMapAdd(map, key3, &x3);

  printf("New Map:\n");
  HashMapPrint(map, printFunction);
  printf("\n\n\n");

  HashMapDestroy(map, false, false);
}

void test5PrintFunction(void) {
  THashMap map = HashMapCreate(3, DEFAULT_HASHMAP_CAPACITY_INCREMENT, hashingFunction,
                               compareKeyFunction);

  int x1 = 124;
  int x2 = 420;
  int x3 = -8520;
  int x4 = 5110;
  int x5 = -987;
  int x6 = 36000;
  int x7 = -100;

  const char* key1 = "pen15";
  const char* key2 = "another";
  const char* key3 = "lolol";
  const char* key4 = "ll12";
  const char* key5 = "loTESTl";
  const char* key6 = "l782Wol";
  const char* key7 = "Pointer";

  HashMapAdd(map, key1, &x1);
  HashMapAdd(map, key2, &x2);
  HashMapAdd(map, key3, &x3);
  HashMapAdd(map, key4, &x4);
  HashMapAdd(map, key5, &x5);
  HashMapAdd(map, key6, &x6);
  HashMapAdd(map, key7, &x7);

  HashMapPrint(map, printFunction);

  HashMapDestroy(map, false, false);
}

void test6SetCapacity(void) {
  THashMap map = HashMapCreate(2, 3, hashingFunction, compareKeyFunction);

  int x1 = 124;
  int x2 = 420;
  int x3 = -8520;

  const char* key1 = "pen15";
  const char* key2 = "another";
  const char* key3 = "lolol";

  HashMapAdd(map, key1, &x1);
  HashMapAdd(map, key2, &x2);

  printf("Map:\n");
  HashMapPrint(map, printFunction);
  printf("\n\n\n");

  printf("Set Capacity to 6:\n");
  HashMapSetCapacity(map, 6);

  HashMapAdd(map, key3, &x3);

  printf("New Map:\n");
  HashMapPrint(map, printFunction);
  printf("\n\n\n");

  HashMapDestroy(map, false, false);
}
