#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct LinkedListStruct* TLinkedList;

typedef bool (*TestFunction)(const void* key, const void* element);

/**
 * @brief Add an element to this LinkedList.
 *
 * @param list The LinkedList to which you want to add an element.
 * .
 * @param element A pointer to the element you want to store. Only the pointer
 *                is stored not the data it points to.
 */
void ListAdd(const TLinkedList list, const void* element);

/**
 * @brief Remove every element in this LinkedList. ListClear is NOT the same as
 *        ListDestroy, because this LinkedList can still be used after it
 *        has been cleared, whereas after calling ListDestroy on it, the LinkedList
 *        should not be used anymore to avoid memory leaks or corruption.
 *
 *
 * @param list The LinkedList you want to clear.
 *
 * @param freeElements true if 'free()' should be appllied to every element, else
 *                     false if no element should be freed.
 */
void ListClear(const TLinkedList list, bool freeElements);

/**
 * @brief Check if any of this LinkedLists elements fulfill the
 *        given test condition.
 *
 *
 * @param list The LinkedList in which you want to search for key.
 *
 * @param key  The pointer to the element you want to look for.
 *
 * @param start Check every element in this LinkedList from index
 *              start to the end.
 *
 * @param test A callback for comparing each element with key.
 *             Returns true if the element at index fulfills
 *             the test condition, else false.
 *
 *
 * @return true if this LinkedList contains key, else false.
 */
bool ListContains(const TLinkedList list, const void* key, size_t start,
                  const TestFunction test);

/**
 * @brief Get a copy of source.
 *
 *
 * @param source The LinkedList you want to copy from.
 *
 *
 * @return A new LinkedList containing all elements from source.
 */
TLinkedList ListCopy(const TLinkedList source);

/**
 * @brief Allocate an empty new LinkedList.
 *
 *
 * @return An empty new LinkedList.
 */
TLinkedList ListCreate(void);

/**
 * @brief Destroy this list.
 *
 *
 * @param list This LinkedList should not be used for any other methods anymore.
 *
 * @param freeElements true if 'free()' should be apllied to every element,
 *                     otherwise false if no element should be freed.
 */
void ListDestroy(TLinkedList list, bool freeElements);

/**
 * @brief Get the element at index from this LinkedList.
 *
 *
 * @param list The LinkedList you want to get an element from.
 *
 * @param index The index at which your desired element is placed.
 *              If the condition 0 =< index < ListSize is not
 *              fulfilled, then NULL gets returned.
 *
 *
 * @return The element at index from this LinkedList, or NULL.
 */
void* ListElementAt(const TLinkedList list, size_t index);

/**
 * @brief Get the first element in this LinkedList.
 *
 *
 * @param list The LinkedList from which you want to get the first
 *             element.
 *
 *
 * @return The first element in this LinkedList, or NULL.
 */
void* ListFirst(const TLinkedList list);

/**
 * @brief This function lets you iterate over every element in this
 *        LinkedList. This is more efficient than calling ListElementAt
 *        for each index.
 *
 *
 * @param list The LinkedList you want to iterate over.
 *
 * @param forEach The callback.
 */
void ListForEach(const TLinkedList list, void (*forEach)(size_t index, void* element));

/**
 * @brief Check if this LinkedList contains key and fulfills the
 *        given test and if it does, return the first found element
 *        from this LinkedList.
 *
 *
 * @param list The LinkedList in which you want to search for key.
 *
 * @param key The pointer to the element you want to look for.
 *
 * @param start Check every element in this LinkedList from index
 *              start to the end.
 *
 * @param test A callback for comparing each element with key.
 *             Returns true if the element at index fulfills
 *             the test condition, else false.
 *
 *
 * @return The element in this LinkedList that fulfills
 *         the test condition.
 */
void* ListGetIfContains(const TLinkedList list, const void* key, size_t start,
                        const TestFunction test);

/**
 * @brief Inserts element at position index in this list.
 *
 *        This increases the length of the list by one and shifts
 *        all objects at or after the index towards the end of
 *        this LinkedList.
 *
 *        The element is only inserted if the condition index < ListSize
 *        is met.
 *
 *
 * @param list The LinkedList you want to insert an element into.
 *
 * @param index The position where you want to insert the specified
 *              element.
 *
 * @param element The element you want to insert into this LinkedList.
 */
void ListInsert(const TLinkedList list, size_t index, const void* element);

/**
 * @param list The LinkedList which should be checked for its emptiness.
 *
 *
 * @return true if this LinkedList IS empty, otherwise false.
 */
bool ListIsEmpty(const TLinkedList list);

/**
 * @param list The LinkedList which should be checked for its emptiness.
 *
 *
 * @return true if this LinkedList IS NOT empty, otherwise false.
 */
bool ListIsNotEmpty(const TLinkedList list);

/**
 * @brief Get the last element in this LinkedList.
 *
 *
 * @param list The LinkedList from which you want to get the last
 *             element.
 *
 *
 * @return The last element in this LinkedList, or NULL.
 */
void* ListLast(const TLinkedList list);

/**
 * @param list The LinkedList you want to print to the console.
 */
void ListPrint(const TLinkedList list);

/**
 * @brief Put an element at index.
 *
 *
 * @param list The LinkedList you want to edit.
 *
 * @param index The index you want to assign a new element to.
 *              If the condition 0 =< index < ListSize is not
 *              fulfilled, then NULL gets returned and no element
 *              gets replaced.
 *
 * @param element The element you want to put at index.
 *
 *
 * @return The old element at index from this LinkedList, or NULL.
 */
void* ListPut(const TLinkedList list, size_t index, const void* element);

/**
 * @brief Remove the element at index from this LinkedList.
 *
 *
 * @param list The LinkedList you want to remove an element of.
 *
 * @param index The index of the element you want to remove.
 *              If the condition 0 =< index < ListSize is not
 *              fulfilled, then NULL gets returned and no element
 *              is removed.
 *
 *
 * @return The removed element, or NULL.
 */
void* ListRemoveAt(const TLinkedList list, size_t index);

/**
 * @param list The LinkedList you want to reverse the order of.
 */
void ListReverse(TLinkedList list);

/**
 * @brief Get the current size of this LinkedList.
 *        This is not ressource expensive, because the current
 *        size of this LinkedList is stored in a variable.
 *
 *
 * @param list The list you want to know the size of.
 *
 *
 * @return Size of this list.
 */
size_t ListSize(const TLinkedList list);

/**
 * @brief This function allocates enough memory for an array
 *        to fit all elements from this LinkedList. Freeing this
 *        memory is the users responsibility.
 *
 *
 * @param list The LinkedList you want to convert to an array.
 *
 *
 * @return An array filled with this LinkedLists elements.
 */
void* ListToArray(const TLinkedList list);

/**
 * @brief Get the first element in this LinkedList, that satisfies
 *        the provided test.
 *
 *
 * @param list The LinkedList from which you want to get the element
 *             that satisfies test.
 *
 * @param start Check every element in this LinkedList from index
 *              start to the end.
 *
 * @param test The test condition the element has to satisfy. Returns
 *             true if the element fulfills test, else false.
 *
 *
 * @return The first element in this LinkedList that satisfies test, or NULL.
 */
void* ListWhere(const TLinkedList list, size_t start,
                bool (*test)(size_t index, const void* element));

#endif  // LINKEDLIST_H
