#include "linked_list.h"

#define VOID_PTR(x)  ((void*) x)
#define UINTPTR_T(x) ((uintptr_t) x)

/***************** Declaration of Private Typedefs *****************/

typedef struct Node TNode;

/***************** Declaration of Private Structs *****************/

struct LinkedListStruct {
  TNode* head;
  TNode* tail;
  size_t size;
};

struct Node {
  TNode* next;
  TNode* previous;
  uintptr_t element;
};

/***************** Declaration of Private Functions *****************/

static TNode* _mallocTNode(void);

static void _ListDestroyRecursive(TNode* head, bool freeElements);

static TNode* _ListTNodeAt(const TLinkedList list, size_t index);

/***************** Definition of Public Functions *****************/

void ListAdd(const TLinkedList list, const void* element) {
  list->size++;

  TNode* next = _mallocTNode();

  next->element = UINTPTR_T(element);
  next->next = NULL;
  next->previous = list->tail;

  list->tail->next = next;
  list->tail = next;
}

void ListClear(const TLinkedList list, bool freeElements) {
  _ListDestroyRecursive(list->head, freeElements);
  if (freeElements) free(VOID_PTR(list->head->element));
  list->head->next = NULL;
  list->size = 0;
}

bool ListContains(const TLinkedList list, const void* key, size_t start,
                  const TestFunction test) {
  if (NULL == list) {
    printf("List is NULL.\n");
    return NULL;
  }

  if (list->size <= start) {
    printf(
        "start is greater than, or equal to the size of this LinkedList.\n"
        "size = %lu\n"
        "start = %lu\n",
        list->size, start);
    return NULL;
  }

  if (NULL == test) {
    printf("test is NULL.\n");
    return NULL;
  }

  if (ListIsEmpty(list)) return NULL;

  TNode* current = _ListTNodeAt(list, start);

  while (1) {
    if (test(key, VOID_PTR(current->element))) return true;
    if (NULL == current->next) break;
    current = current->next;
  }

  return false;
}

TLinkedList ListCopy(const TLinkedList source) {
  TLinkedList copy = ListCreate();
  copy->size = source->size;

  TNode* current = source->head->next;
  TNode* next;

  for (; NULL != current; current = current->next) {
    next = _mallocTNode();
    next->element = current->element;

    copy->tail->next = next;
    copy->tail = next;
  }

  return copy;
}

TLinkedList ListCreate(void) {
  TLinkedList list = (TLinkedList) calloc(1, sizeof(struct LinkedListStruct));

  list->head = _mallocTNode();
  list->head->next = NULL;
  list->head->previous = NULL;
  list->head->element = 0;

  list->tail = list->head;

  return list;
}

void ListDestroy(TLinkedList list, bool freeElements) {
  _ListDestroyRecursive(list->head, freeElements);
  if (freeElements) free(VOID_PTR(list->head->element));
  free(list->head);
  free(list);
}

void* ListElementAt(const TLinkedList list, size_t index) {
  TNode* current = _ListTNodeAt(list, index);
  if (NULL == current) return NULL;
  return VOID_PTR(current->element);
}

void* ListFirst(const TLinkedList list) { return VOID_PTR(list->head->next); }

void ListForEach(const TLinkedList list, void (*forEach)(size_t index, void* element)) {
  if (NULL == forEach) {
    printf("forEach is NULL.\n");
    return;
  }

  if (NULL == list) {
    printf("List is NULL.\n");
    return;
  }

  if (ListIsEmpty(list)) return;

  TNode* current = list->head->next;
  size_t index = 0;

  while (1) {
    forEach(index, VOID_PTR(current->element));
    if (NULL == current->next) break;
    index++;
    current = current->next;
  }
}

void* ListGetIfContains(const TLinkedList list, const void* key, size_t start,
                        const TestFunction test) {
  if (NULL == list) {
    printf("List is NULL.\n");
    return NULL;
  }

  if (list->size <= start) {
    printf(
        "start is greater than, or equal to the size of this LinkedList.\n"
        "size = %lu\n"
        "start = %lu\n",
        list->size, start);
    return NULL;
  }

  if (NULL == test) {
    printf("test is NULL.\n");
    return NULL;
  }

  if (ListIsEmpty(list)) return NULL;

  TNode* current = _ListTNodeAt(list, start);

  while (1) {
    if (test(key, VOID_PTR(current->element))) return VOID_PTR(current->element);
    if (NULL == current->next) break;
    current = current->next;
  }

  return NULL;
}

void ListInsert(const TLinkedList list, size_t index, const void* element) {
  TNode* destination = _ListTNodeAt(list, index);
  if (NULL == destination) return;

  TNode* insert = _mallocTNode();
  insert->element = UINTPTR_T(element);

  insert->next = destination;
  destination->previous->next = insert;

  insert->previous = destination->previous;
  destination->previous = insert;
}

bool ListIsEmpty(const TLinkedList list) { return (0 == list->size); }

bool ListIsNotEmpty(const TLinkedList list) { return (0 != list->size); }

void* ListLast(const TLinkedList list) { return VOID_PTR(list->tail); }

void ListPrint(const TLinkedList list) {
  if (NULL == list) {
    printf("List is NULL.\n");
    return;
  }

  TNode* current = list->head->next;
  size_t index;

  printf("HEAD:\tprevious: %p\t\t\tcurrent: %p\tnext: %p\telement: %p\n",
         VOID_PTR(list->head->previous), VOID_PTR(list->head), VOID_PTR(list->head->next),
         VOID_PTR(list->head->element));

  for (index = 0; NULL != current->next; index++) {
    printf("[%lu]:\tprevious: %p\tcurrent: %p\tnext: %p\telement: %p\n", index,
           VOID_PTR(current->previous), VOID_PTR(current), VOID_PTR(current->next),
           VOID_PTR(current->element));
    current = current->next;
  }

  printf("[%lu]:\tprevious: %p\tcurrent: %p\tnext: %p\t\telement: %p\n", index,
         VOID_PTR(current->previous), VOID_PTR(current), VOID_PTR(current->next),
         VOID_PTR(current->element));
}

void* ListPut(const TLinkedList list, size_t index, const void* element) {
  TNode* current = _ListTNodeAt(list, index);

  uintptr_t old = current->element;
  current->element = UINTPTR_T(element);

  return VOID_PTR(old);
}

void* ListRemoveAt(const TLinkedList list, size_t index) {
  TNode* current = _ListTNodeAt(list, index);
  if (NULL == current) return NULL;

  uintptr_t removed = current->element;

  if (NULL != current->next) current->next->previous = current->previous;
  current->previous->next = current->next;

  list->size--;
  free(current);

  return VOID_PTR(removed);
}

void ListReverse(TLinkedList list) {
  TNode* current = list->tail;
  TNode* previous = current->previous;
  TNode* next = list->head;

  list->head->next = list->tail;

  for (size_t i = 1; i < list->size; i++) {
    current->next = previous;
    current->previous = next;

    next = current;
    current = previous;
    previous = current->previous;
  }

  current->next = NULL;
  current->previous = next;
  list->tail = current;
}

size_t ListSize(const TLinkedList list) { return list->size; }

void* ListToArray(const TLinkedList list) {
  if (0 == list->size) return NULL;

  uintptr_t* array = (uintptr_t*) calloc(list->size, sizeof(uintptr_t));

  TNode* current = list->head->next;
  for (size_t i = 0; NULL != current; i++, current = current->next)
    array[i] = current->element;

  return VOID_PTR(array);
}

void* ListWhere(const TLinkedList list, size_t start,
                bool (*test)(size_t index, const void* element)) {
  if (NULL == list) {
    printf("List is NULL.\n");
    return false;
  }

  if (list->size <= start) {
    printf(
        "start is greater than, or equal to the size of this LinkedList.\n"
        "size = %lu\n"
        "start = %lu\n",
        list->size, start);
    return false;
  }

  if (NULL == test) {
    printf("test is NULL.\n");
    return false;
  }

  if (ListIsEmpty(list)) return false;

  size_t index = start;
  TNode* current = _ListTNodeAt(list, index);

  while (1) {
    if (test(index, VOID_PTR(current->element))) return VOID_PTR(current->element);
    if (NULL == current->next) break;
    index++;
    current = current->next;
  }

  return NULL;
}

/***************** Definition of Private Functions *****************/

static TNode* _mallocTNode(void) { return (TNode*) malloc(sizeof(TNode)); }

static void _ListDestroyRecursive(TNode* head, bool freeElements) {
  if (NULL == head->next) return;
  _ListDestroyRecursive(head->next, freeElements);
  if (freeElements) free(VOID_PTR(head->element));
  free(head->next);
}

static TNode* _ListTNodeAt(const TLinkedList list, size_t index) {
  TNode* current;

  if (index <= list->size / 2)
  // start at head
  {
    current = list->head->next;
    for (; NULL != current && index > 0; index--) current = current->next;
  } else
  // start at tail
  {
    current = list->tail;
    for (size_t i = 0; NULL != current && (list->size - 1) - index > i; i++)
      current = current->previous;
  }

  return current;
}